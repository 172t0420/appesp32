package com.polo.myapplication

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var database: FirebaseDatabase
    private lateinit var databaseReference: DatabaseReference
    private var dataset: ArrayList<Entry> = ArrayList()

    private var bandera = true
    private var i = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        database = FirebaseDatabase.getInstance()
        databaseReference = database.getReference("Sensores")
        datos()
    }

    private fun datos(){
        databaseReference.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                Log.e("cancel", error.toString())
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                val list = ArrayList<ModelDatos>()
                var sizeData:Int = snapshot.childrenCount.toInt()
                for(data in snapshot.children){

                    val model = data.getValue(ModelDatos::class.java)
                    val dato1 = data.child("Distancia").value.toString()
                    print(dato1)

                    var entry = Entry(i.toFloat(), dato1.toFloat())

                    if (bandera) {
                        dataset.add(entry)
                        i++
                    }else if(i == sizeData) {
                        dataset.add(entry)
                        i++
                    }
                    grafica()
                }
            }
        })
    }

    private fun grafica() {
        var lineDataSet: LineDataSet = LineDataSet(dataset,"Distancia")
        var iLineDataSets:ArrayList<ILineDataSet> = ArrayList()
        iLineDataSets.add(lineDataSet)

        var lineData: LineData = LineData(iLineDataSets)
        graf.data = lineData
        graf.invalidate()
        graf.setNoDataText("Datos no disponibles")

        lineDataSet.color = Color.rgb(255, 255, 255)
        lineDataSet.setCircleColor(Color.rgb(113, 170, 237))
        lineDataSet.setDrawCircles(true)
        lineDataSet.lineWidth = 6f
        lineDataSet.circleRadius = 7f
        lineDataSet.valueTextSize = 12f
        lineDataSet.valueTextColor = Color.rgb(255, 255, 255)
    }
}